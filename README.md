# concatRequirementsCSV

A small command-line utility to consume a CSV file exported from a source 
(like a database) where there are many distict rows that you want to concatenat 
one field among those many matches into a single field in an output CSV.

For example, to take all the customer requirements (stored one row per 
requirement per customer in old mettrack database) and import into new 
MetTeam database we need to consolodate all of those requrements into a field
by customer location.  This attemtps to do that.  Does not alter original CSV 
but generates a new CSV.

## TL;DR

```bash
$ ls
sample.csv
$ concatRequirementsCSV -file sample.csv

Preparing to write new file.
==========
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
New file generated: sample_concatReqs.csv
==========

$ls
sample.csv sample_concatReqs.csv
```

## Get the software

```go
$ go get gitlab.com/thanateros/concatRequirementsCSV
```

## Install the software

```bash
$ cd $GOPATH/src/gitlab.com/thanateros/concatRequirementsCSV
$ go install
```

## Use the software

The command takes one flag: `-file path/to/csv/file`.

Consumes a CSV file consiting of `CustomerID` and `Requirements` columns.
Generates a new CSV file containing a single unique entry for `CustomerID`
and a `Requirements` column that has concatenated together in a single
field all the requirements from separate rows in origina CSV for that 
customer i.d..
It does not alter the source file.