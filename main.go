package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

func main() {

	outputCSV := [][]string{}

	inputCSVFilePtr := flag.String("file", "", "path to input CSV file.\nExample: 'concatRequirementsCSV -file path/to/csv/file'.")

	flag.Parse()

	outputCSVfile := strings.TrimSuffix(*inputCSVFilePtr, filepath.Ext(*inputCSVFilePtr)) + "_concatReqs.csv"

	csvFile, _ := os.Open(*inputCSVFilePtr)
	defer csvFile.Close()

	reader := csv.NewReader(bufio.NewReader(csvFile))
	reader.LazyQuotes = true

	// outputRow := []string{}
	m := make(map[string]string)

	for {
		line, err := reader.Read()

		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatalf("There was an error: %v\n", err)
			fmt.Println("Aborting. \nType 'concatRequirementsCSV -h' for assistance.")
			os.Exit(1)
		}

		m[line[0]] += (line[1] + "\n")

	}

	mSort := make([]string, 0, len(m))
	for k := range m {
		mSort = append(mSort, k)
	}
	sort.Strings(mSort)

	for _, val := range mSort {
		newRow := []string{val, m[val]}
		outputCSV = append(outputCSV, newRow)
	}

	outputFile, err := os.Create(outputCSVfile)
	checkError("Cannot create file: ", err)
	defer outputFile.Close()

	writer := csv.NewWriter(outputFile)
	defer writer.Flush()

	for _, value := range outputCSV {
		err := writer.Write(value)
		checkError("Cannot write to file: ", err)
	}

	fmt.Printf("==========\nCompleted concatenating customer requirements.\nNew file generated: %v\n==========\n", outputCSVfile)
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}
